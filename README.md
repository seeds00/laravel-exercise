# Laravel Exercise

## Exercise documentation
Please refer to the summary documentation page here: [README.md](src/README.md)


### Accessing project
```
docker-compose up -d --build
```

### Web development
`http://localhost:81/`

### Create Laravel Project
`docker-compose exec app composer create-project laravel/laravel src`

#### Composer update
`docker-compose exec app composer update`

