#Rally Exercise

## Basic CS

### A

Here you can find migrations that I would implement for the purpose of the exercise:
[nba_players](database/migrations/2021_08_07_082831_create_nba_players_table.php),
[nba_teams](database/migrations/2021_08_07_082905_create_nba_teams_table.php),
[nba_games](database/migrations/2021_08_07_082924_create_nba_games_table.php).

### B

By accessing route [exercise-b](http://localhost:81/exercise-b) you can see the output of the solution.
Method is called `deleteFromDir`, found here:
[BasicCSController](app/Http/Controllers/BasicCSController.php),

### C
By accessing route [exercise-c](http://localhost:81/exercise-c) you can see the output of the solution.
Method is called `sort`, found here:
[BasicCSController](app/Http/Controllers/BasicCSController.php),

About the expected time, quicksort is a well known theorem which can go both ways or some value in between:

- Worst Case: The worst case occurs when the partition process always picks greatest or smallest element as pivot.
  If it happens that the last element is always picked as pivot, the worst case would occur when the array is already 
  sorted in increasing or decreasing order. Following is recurrence for worst case.
  In this case, 10 billion times would be calculated by:  10<sup>10</sup> * θ(n<sup>2</sup>)
- Best Case: The best case occurs when the partition process always picks the middle element as pivot.
  Following is recurrence for best case.
  In this case, 10 billion times would be calculated by:  10<sup>10</sup> * (2T(n/2) + θ(n))

### D
By accessing route [exercise-d](http://localhost:81/exercise-d) you can see the output of the solution.
Method is called `sortPowers`, found here:
[BasicCSController](app/Http/Controllers/BasicCSController.php),

About the expected time on my machine, the process of sort 10000 powers lasts around 175ms.

The principle for this exercise is the same of the previous. The main difference is on the comparison of the pivot power with the others.
The comparison only occurs after converting different base numbers to the same one and calculate the exponent number
using: `calculatedExponent = comparisonExponent * log(comparisonBase)/log(pivotBase)`.
After this, it is possible to quickly compare if its bigger os smaller than the pivot exponent value.

## Practical

### Routing
- [users](http://localhost:81/users)
- [posts](http://localhost:81/posts)
- [posts/search/?title=repellat](http://localhost:81/posts/search/?title=repellat)
- [comments](http://localhost:81/comments)

### Database
Implementation of some indexes in order to optimize searches by:

- Users by email
- Posts by title
- Comments by Post ID

### Sync users, posts and comments
Implementation of [seeders](database/seeders) in order to cache on mysql database the first 10 users, their posts and comments.

Run `docker-compose exec app php artisan migrate:fresh --seed`
to sync the information needed.

### Trait
Implementation of [ColumnValidationTrait](app/Traits/ColumnValidationTrait.php), allowing searches only if columns exist.

### Repositories
Added the [Repository](app/Repositories) design pattern to optimize code management and in order to make changes just on a single place.
Repositories follows an [interface contract](app/Repositories/Contracts) that prevent breaking the code if a method changes signature or return value.
The abstract class avoids code repetition.

### Future implementations
- unit and integration testing: in order to keep the exercises on 5-6 hours was not able to add them properly.
- pipeline to deploy the api to development, staging and production: 
  would allow the team to properly test the api on a production-like environment before the deploy.
- optimize other main searched queries: analyse if there are any other queries that need to improve its performance. 
