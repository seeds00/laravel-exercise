<?php

namespace App\Repositories;

use App\Models\Comment;
use App\Repositories\Contracts\CommentRepositoryInterface;

class CommentRepository extends AbstractRepository implements CommentRepositoryInterface
{
    protected mixed $model = Comment::class;
}
