<?php

namespace App\Repositories;

abstract class AbstractRepository
{
    protected mixed $model;

    public function __construct()
    {
        $this->model = $this->resolveModel();
    }

    public function get($request): object
    {
        $response = $this->model;
        if ($request) {
            foreach ($request->all() as $param => $value) {
                $this->model->validateColumns($param);
                $response = $response
                    ->where($param, $value);
            }
        }
        return $response->get();
    }

    protected function resolveModel(): object
    {
        return app($this->model);
    }
}
