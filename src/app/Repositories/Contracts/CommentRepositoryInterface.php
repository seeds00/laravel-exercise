<?php

namespace App\Repositories\Contracts;

interface CommentRepositoryInterface
{
    public function get($request);
}
