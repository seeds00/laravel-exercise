<?php

namespace App\Repositories\Contracts;

interface PostRepositoryInterface
{
    public function get($request);
    public function search($request);
}
