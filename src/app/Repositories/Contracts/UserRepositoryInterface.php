<?php

namespace App\Repositories\Contracts;

interface UserRepositoryInterface
{
    public function get($request);
}
