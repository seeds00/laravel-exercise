<?php

namespace App\Repositories;

use App\Models\Post;
use App\Repositories\Contracts\PostRepositoryInterface;

class PostRepository extends AbstractRepository implements PostRepositoryInterface
{
    protected mixed $model = Post::class;

    /**
     * @throws \Exception
     */
    public function search($request): object
    {
        if (!$request->get('title')) {
            throw new \Exception("Please insert title param.");
        }

        $response = $this->model;

        foreach ($request->all() as $param => $value) {
            if ($param === 'title') {
                $response = $response
                    ->where($param, 'like', '%' . $value . '%');
            } else {
                $this->model->validateColumns($param);
                $response = $response
                    ->where($param, $value);
            }
        }

        return $response->get();
    }
}
