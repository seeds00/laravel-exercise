<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\CommentRepositoryInterface;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index(CommentRepositoryInterface $model, Request $request)
    {
        return $model->get($request);
    }
}
