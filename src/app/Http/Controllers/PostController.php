<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\PostRepositoryInterface;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(PostRepositoryInterface $model, Request $request)
    {
        return $model->get($request);
    }

    public function search(PostRepositoryInterface $model, Request $request)
    {
        return $model->search($request);
    }
}
