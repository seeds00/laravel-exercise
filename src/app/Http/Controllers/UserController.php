<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(UserRepositoryInterface $model, Request $request)
    {
        return $model->get($request);
    }
}
