<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BasicCSController extends Controller
{

    public function deleteFromDir(): bool
    {
        $dir = public_path('/dirToScan');

        $this->deleteDir($dir);

        mkdir($dir);

        $this->createFiles($dir);

        $allFilesBeforeDelete = $this->findAllFiles($dir);

        foreach ($allFilesBeforeDelete as $filePath) {
            if (strpos($filePath, '0aH') !== false) {
                $this->deleteFilesByString($filePath);
            }

        }

        $allFilesAfterDelete = $this->findAllFiles($dir);

        dump($allFilesBeforeDelete);
        dump($allFilesAfterDelete);
        return true;
    }

    private function createFiles($dir): void
    {
        $folderArray = ['/a', '/b', '/c', '/d'];
        $filePathToDelete = '/0aH-file';
        $filePathToKeep = '/file';

        foreach ($folderArray as $folderName) {
            mkdir($dir . $folderName);
            if(rand(0, 1) == 1) {
                fopen($dir . $folderName . $filePathToDelete, "w");
            }
            if(rand(0, 1) == 1) {
                fopen($dir . $folderName . $filePathToKeep, "w");
            }

            foreach ($folderArray as $subFolderName) {
                mkdir($dir . $folderName . $subFolderName);
                if(rand(0, 1) == 1) {
                    fopen($dir . $folderName . $subFolderName . $filePathToDelete, "w");
                }
                if(rand(0, 1) == 1) {
                    fopen($dir . $folderName . $subFolderName . $filePathToKeep, "w");
                }

                foreach ($folderArray as $subSubFolderName) {
                    mkdir($dir . $folderName . $subFolderName . $subSubFolderName);
                    if(rand(0, 1) == 1) {
                        fopen($dir . $folderName . $subFolderName . $subSubFolderName . $filePathToDelete, "w");
                    }
                    if(rand(0, 1) == 1) {
                        fopen($dir . $folderName . $subFolderName . $subSubFolderName . $filePathToKeep, "w");
                    }
                }
            }
        }
    }

    private function deleteDir($dir): string
    {
        if (!file_exists($dir)) { return true; }

        if (!is_dir($dir)) { return unlink($dir); }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') { continue; }

            if (!$this->deleteDir($dir . DIRECTORY_SEPARATOR . $item)) { return false; }
        }
        return rmdir($dir);
    }

    private function findAllFiles($dir): array
    {
        $rootDir = scandir($dir);
        $result = [];
        foreach($rootDir as $fileValue)
        {
            if($fileValue === '.' || $fileValue === '..') {continue;}
            if(is_file("$dir/$fileValue")) {$result[]="$dir/$fileValue";continue;}
            foreach($this->findAllFiles("$dir/$fileValue") as $subFilesValue)
            {
                $result[]=$subFilesValue;
            }
        }
        return $result;
    }

    private function deleteFilesByString($str)
    {
        unlink($str);
    }

    public function sort(): bool
    {
        $numbersArray = [];

        for ($i = 0; $i < 11; $i ++) {
            $numbersArray[] = rand(0, 100);
        }

        dump($numbersArray);
        dump($this->quickSort($numbersArray));
        return true;
    }

    private function quickSort($my_array): array
    {
        $loe = $gt = [];
        if(count($my_array) < 2)
        {
            return $my_array;
        }
        $pivot_key = key($my_array);
        $pivot = array_shift($my_array);
        foreach($my_array as $val)
        {
            if($val <= $pivot)
            {
                $loe[] = $val;
            }elseif ($val > $pivot)
            {
                $gt[] = $val;
            }
        }
        return array_merge($this->quickSort($loe),array($pivot_key=>$pivot),$this->quickSort($gt));
    }

    public function sortPowers(): bool
    {
        $numbersArray = [];
        $minNumber = 100;
        $maxNumber = 10000;
        $numberOfPowers = 10000;

        for ($i = 0; $i < $numberOfPowers; $i ++) {
            $numbersArray[] = [
                rand($minNumber, $maxNumber),
                rand($minNumber, $maxNumber)
            ];
        }

        $starttime = microtime(true);

        $sortedPowers = $this->quickSortPowers($numbersArray);

        $endtime = microtime(true);

        $timediff = $endtime - $starttime;
        dump($timediff);
        dump($numbersArray);
        dump($sortedPowers);

        return true;
    }

    private function quickSortPowers($my_array): array
    {
        $loe = $gt = [];
        if(count($my_array) < 2)
        {
            return $my_array;
        }
        $pivot_key = key($my_array);
        $pivotBase = $my_array[0][0];
        $pivotExponent = $my_array[0][1];
        array_shift($my_array);

        foreach($my_array as $val)
        {
            $valBase = $val[0];
            $valExponent = $val[1];

            if($pivotExponent > ($valExponent*log($valBase)/log($pivotBase)))
            {
                $loe[] = [$valBase, $valExponent];
                continue;
            }
            if ($pivotExponent <= ($valExponent*log($valBase)/log($pivotBase)))
            {
                $gt[] = [$valBase, $valExponent];
            }
        }
        return array_merge($this->quickSortPowers($loe),array($pivot_key=>[$pivotBase, $pivotExponent]),$this->quickSortPowers($gt));
    }
}
