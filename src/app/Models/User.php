<?php

namespace App\Models;

use App\Traits\ColumnValidationTrait;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use ColumnValidationTrait;

    protected $fillable = [
        'id',
        'name',
        'username',
        'email',
        'address',
        'phone',
        'website',
    ];

    public function posts(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Post::class);
    }

}
