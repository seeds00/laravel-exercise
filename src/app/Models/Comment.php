<?php

namespace App\Models;

use App\Traits\ColumnValidationTrait;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use ColumnValidationTrait;

    protected $fillable = [
        'id',
        'post_id',
        'name',
        'email',
        'body'
    ];

    public function post(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Post::class);
    }
}
