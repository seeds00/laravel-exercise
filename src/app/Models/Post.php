<?php

namespace App\Models;

use App\Traits\ColumnValidationTrait;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use ColumnValidationTrait;

    protected $fillable = [
        'id',
        'user_id',
        'title',
        'body',
        'body'
    ];


    public function comments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Comment::class);
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
