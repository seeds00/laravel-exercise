<?php

namespace App\Traits;

trait ColumnValidationTrait
{

    public function validateColumns($column)
    {
        if (!in_array($column, $this->fillable)) {
            throw new \Exception($column . ' does not exist');
        }
    }
}
