<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNbaGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nba_games', function (Blueprint $table) {
            $table->id();
            $table->dateTime('game_time')->index('idx_game_time');
            $table->integer('home_team_id');
            $table->integer('away_team_id');
            $table->json('partials');
            $table->string('final_result');
            $table->index(['home_team_id', 'away_team_id'], 'idx_team_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nba_games');
    }
}
