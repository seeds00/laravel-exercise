<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNbaPlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nba_players', function (Blueprint $table) {
            $table->id();
            $table->integer('team_id');
            $table->string('name')->index('idx_player_name');
            $table->integer('points');
            $table->integer('rebounds');
            $table->integer('assists');
            $table->integer('blocks');
            $table->integer('steals');
            $table->integer('turnovers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nba_players');
    }
}
