<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $url= 'https://jsonplaceholder.typicode.com/users';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        $users = json_decode($result);

        foreach ($users as $user) {
            DB::table('users')->insert([
                'name' => $user->name,
                'username' => $user->username,
                'email' => $user->email,
                'address' => json_encode($user->address),
                'phone' => $user->phone,
                'website' => $user->website,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

    }
}
