<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $url= 'https://jsonplaceholder.typicode.com/comments';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        $comments = json_decode($result);

        foreach ($comments as $comment) {
            if ($comment->postId < 50) {
                DB::table('comments')->insert([
                    'post_id' => $comment->postId,
                    'name' => $comment->name,
                    'email' => $comment->email,
                    'body' => $comment->body,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }
        }
    }
}
